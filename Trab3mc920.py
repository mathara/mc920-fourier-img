#!/usr/bin/python
import cv2
import sys
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm 
from matplotlib.pyplot import figure

## Passa Alta com raio como parâmetro
def Passa_alta(r):
	#gerando a máscara de acordo com o raio recebido
	mask = np.ones((rows, cols), np.uint8)
	x, y = np.ogrid[:rows, :cols]
	mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
	mask[mask_area] = 0

	#aplicando o processo na transformada
	fmask = fshift * mask
	f_ishift = np.fft.ifftshift(fmask)
	img_back = np.fft.ifft2(f_ishift)
	img_back = np.abs(img_back)

	#impressão da mascára
	plt.imshow(mask, cmap = 'gray'), plt.axis('off')
	plt.savefig('hp_mask_['+str(r)+'].png', bbox_inches='tight', dpi=135)

	#impressão da máscara aplicada
	plt.imshow(np.log(1+np.abs(fmask)), cmap = 'gray'), plt.axis('off')
	plt.savefig('hp_fmask_['+str(r)+'].png', bbox_inches='tight', dpi=135)

	#impressão da inversa
	plt.imshow(np.log(1+np.abs(f_ishift)), cmap = 'gray'), plt.axis('off')
	plt.savefig('hp_fishift_['+str(r)+'].png', bbox_inches='tight', dpi=135)

	#impressão da imagem final
	plt.imshow(img_back, cmap = 'gray'), plt.axis('off')
	plt.savefig('hp_img_back_['+str(r)+'].png', bbox_inches='tight', dpi=135)

## Passa Baixa com raio de parâmetro
def Passa_baixa(r):
	#gerando a máscara de acordo com o raio recebido
	mask = np.zeros((rows, cols), np.uint8)
	x, y = np.ogrid[:rows, :cols]
	mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
	mask[mask_area] = 1

	#aplicando o processo na transformada
	fmask = fshift * mask
	f_ishift = np.fft.ifftshift(fmask)
	img_back = np.fft.ifft2(f_ishift)
	img_back = np.abs(img_back)

	#impressão da mascára
	plt.imshow(mask, cmap = 'gray'), plt.axis('off')
	plt.savefig('lp_mask_['+str(r)+'].png', bbox_inches='tight', dpi=135)

	#impressão da máscara aplicada
	plt.imshow(np.log(1+np.abs(fmask)), cmap = 'gray'), plt.axis('off')
	plt.savefig('lp_fmask_['+str(r)+'].png', bbox_inches='tight', dpi=135)

	#impressão da inversa
	plt.imshow(np.log(1+np.abs(f_ishift)), cmap = 'gray'), plt.axis('off')
	plt.savefig('lp_fishift_['+str(r)+'].png', bbox_inches='tight', dpi=135)

	#impressão da imagem final
	plt.imshow(img_back, cmap = 'gray'), plt.axis('off')
	plt.savefig('lp_img_back_['+str(r)+'].png', bbox_inches='tight', dpi=135)


## Passsa Banda, raio interno e externo como parâmtros
def Passa_banda(r_in, r_out):
	#gerando a máscara de acordo com os raios recebidos
	mask = np.zeros((rows, cols), np.uint8)
	x, y = np.ogrid[:rows, :cols]
	mask_area = np.logical_and(((x - center[0]) ** 2 + (y - center[1]) ** 2 >= r_in ** 2),
			((x - center[0]) ** 2 + (y - center[1]) ** 2 <= r_out ** 2))
	mask[mask_area] = 1

	#aplicando o processo na transformada
	fmask = fshift * mask
	f_ishift = np.fft.ifftshift(fmask)
	img_back = np.fft.ifft2(f_ishift)
	img_back = np.abs(img_back)

	#impressão da mascára
	plt.imshow(mask, cmap = 'gray'), plt.axis('off')
	plt.savefig('bp_mask_['+str(r_in)+']_['+str(r_out)+'].png', bbox_inches='tight', dpi=135)

	#impressão da máscara aplicada
	plt.imshow(np.log(1+np.abs(fmask)), cmap = 'gray'), plt.axis('off')
	plt.savefig('bp_fmask_['+str(r_in)+']_['+str(r_out)+'].png', bbox_inches='tight', dpi=135)

	#impressão da inversa
	plt.imshow(np.log(1+np.abs(f_ishift)), cmap = 'gray'), plt.axis('off')
	plt.savefig('bp_fishift_['+str(r_in)+']_['+str(r_out)+'].png', bbox_inches='tight', dpi=135)

	#impressão da imagem final
	plt.imshow(img_back, cmap = 'gray'), plt.axis('off')
	plt.savefig('bp_img_back_['+str(r_in)+']_['+str(r_out)+'].png', bbox_inches='tight', dpi=135)

#compressão tendo o valor de corte como parâmetro
def compressao(v_corte):
	# processar a imagem para realizar o corte das frquências
	fcorte = fshift
	fcorte[np.abs(fcorte) < v_corte] = 0
	f_ishift = np.fft.ifftshift(fcorte)
	img_back = np.fft.ifft2(f_ishift)
	img_back = np.abs(img_back)

	#impressão da compressão da imagem
	plt.imshow(img_back, cmap = 'gray'), plt.axis('off')
	plt.savefig('corte_img_back_['+str(v_corte)+'].png', bbox_inches='tight', dpi=135)


#a imagem que foi passada como parâmetro
nome_imagem = sys.argv[1]

# creating a image object (main image)  
img = cv2.imread(nome_imagem, 0)

# processar a imagem para entender o processo
f = np.fft.fft2(img)
fshift = np.fft.fftshift(f)
magnitude_spectrum = 20*np.log(np.abs(fshift))
f_ishift = np.fft.ifftshift(fshift)
img_back = np.fft.ifft2(f_ishift)
img_back = np.abs(img_back)

#impressão da imagem
plt.imshow( np.log(1+np.abs(f)) , cmap = 'gray'),plt.axis('off')
plt.savefig("f.png", bbox_inches='tight', dpi=135)

plt.imshow(np.log(1+np.abs(fshift)) , cmap = 'gray'), plt.axis('off')
plt.savefig("fshift.png", bbox_inches='tight', dpi=135)

plt.imshow(np.log(1+np.abs(magnitude_spectrum)), cmap = 'gray'), plt.axis('off')
plt.savefig("magnitude_spectrum.png", bbox_inches='tight', dpi=135)

plt.imshow(np.log(1+np.abs(f_ishift)), cmap = 'gray'), plt.axis('off')
plt.savefig("f_ishift.png", bbox_inches='tight', dpi=135)

plt.imshow(img_back, cmap = 'gray'), plt.axis('off')
plt.savefig("img_back.png", bbox_inches='tight', dpi=135)

# centrando o círculo
rows, cols = img.shape
crow, ccol = int(rows / 2), int(cols / 2)
center = [crow, ccol]

## Passa Alta
Passa_alta(200)
Passa_alta(256)

## Passa Baixa
Passa_baixa(200)
Passa_baixa(256)

## Passsa Banda
Passa_banda(150, 220)
Passa_banda(100, 256) 

# Compressão
compressao(100000)
compressao(500000)
compressao(1000000)