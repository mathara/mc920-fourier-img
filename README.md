# Execução
O programa pode ser executado através do script $Trab3mc920.py$.
O script recebe como argumento uma imagem de entrada que se encontra no mesmo diretório que o arquivo, o algoritmo irá executar todas as transformações, salvar as imagens resultantes no mesmo diretório.
Para rodar o script podemos fazer dessa forma :

```python
python3 Trab3mc920.py city.png
```

# output
O output será salvo na mesma pasta e com o seguinte nome :
[t]\_mask\_[r], [t]\_fmask\_[r], [t]\_fishift\_[r], [t]\_img\_back\_[r], 
sendo que [t] pode variar entre os seguintes valores hp (passa alta), lp (passa baixa) e bp (passa banda);
[r] é o raio da imagem gerada, sendo que nas imagens com bp (passa banda), teremos dois valores de raios, interno e externo.
O nome da imagem deve vir acompanhada do tipo, caso contrário, não será possível rodar o script e realizar a passagem de todos os filtros.